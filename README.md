# padatrad.editionsburnaout.fr

Espace du site web [editionsburnaout.fr](https://editionsburnaout.fr) dédié à l'hébergement de notre logiciel de traduction collaborative [Padatrad](https://gitlab.com/editionsburnaout.fr/padatrad).

Un jour, peut-être que ce dépôt contiendra une interface pour que chacun et chacune puisse créer et administrer ses propres instances de Padatrad sur notre serveur. Mais ça c'est quand on aura le temps de s'en occuper :)

**Licence :** GNU-GPLv3.